import { MicroService } from 'microservice';
import { Requests } from 'ootils';
import * as assert from 'assert';
import * as url from 'url';
import * as mocha from 'mocha';
import * as request from 'supertest';
import { main } from '../index';
let service: MicroService;

describe('routes', () => {

    before(async () => {
        service = await main();
    });

    beforeEach(() => {
        service.stop();
        service.clearCache(true);
        service.start(Math.floor(Math.random() * 8999) + 1000);
    });
    afterEach(() => {
        service.stop();
    });

    describe('spawn', () => {

        it('microservice-template', async () => {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({query: {
                package_name: 'gitlab:node-microservice/template',
                package_dir: 'microservice-template',
                run_args: JSON.stringify({port: port})
            }});
            const res = await request(service.server).get('/spawn' + query).expect(200);
            const pid = JSON.parse(res.text).data.pid;
            assert.notEqual(pid, null);
            const endpoint = url.format({
                port: port,
                protocol: 'http',
                hostname: '127.0.0.1',
                pathname: '/'
            });

            // Give the microservice a few seconds to start up
            console.log('Waiting for process to restart...')
            await new Promise((resolve, _) => setTimeout(resolve, 5000));

            // Test the service
            const res2 = await Requests.get(endpoint);
            assert.equal(res2, 'Hello World');
        }).timeout(120000);

        it('keep alive', async () => {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({query: {
                package_name: 'gitlab:node-microservice/template',
                package_dir: 'microservice-template',
                run_args: JSON.stringify({port: port})
            }});
            const res = await request(service.server).get('/spawn' + query).expect(200);
            const pid = JSON.parse(res.text).data.pid;
            assert.notEqual(pid, null);
            const endpoint = url.format({
                port: port,
                protocol: 'http',
                hostname: '127.0.0.1',
                pathname: '/'
            });

            // Give the microservice a few seconds to start up
            console.log('Waiting for process to restart...')
            await new Promise((resolve, _) => setTimeout(resolve, 5000));

            // Test the service
            const res2 = await Requests.get(endpoint);
            assert.equal(res2, 'Hello World');

            // Kill the microservice
            console.log('Killing process...');
            process.kill(pid);

            // Give the microservice a few seconds to start up
            console.log('Waiting for process to restart...')
            await new Promise((resolve, _) => setTimeout(resolve, 5000));

            // Test the service again
            const res3 = await Requests.get(endpoint);
            assert.equal(res3, 'Hello World');
        }).timeout(120000);

        it('keep alive not', async () => {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({query: {
                package_name: 'gitlab:node-microservice/template',
                package_dir: 'microservice-template',
                run_args: JSON.stringify({port: port}),
                keepalive: null
            }});
            const res = await request(service.server).get('/spawn' + query).expect(200);
            const pid = JSON.parse(res.text).data.pid;
            assert.notEqual(pid, null);
            const endpoint = url.format({
                port: port,
                protocol: 'http',
                hostname: '127.0.0.1',
                pathname: '/'
            });

            // Test the service
            const res2 = await Requests.get(endpoint);
            assert.equal(res2, 'Hello World');

            // Kill the microservice
            console.log('Killing process...');
            process.kill(pid);

            // Give the microservice a few seconds to start up
            console.log('Waiting for process to restart...')
            await new Promise((resolve, _) => setTimeout(resolve, 5000));

            // Test the service again
            let errored = false;
            try {
                const res3 = await Requests.get(endpoint);
            } catch(err) {
                errored = true;
            } finally {
                assert.equal(errored, true);
            }
        }).timeout(120000);

        it('non-existent module', async () => {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({query: {
                package_name: 'asdfasdf:asdfasdf',
                run_args: JSON.stringify({port: port})
            }});
            await request(service.server).get('/spawn' + query).expect(500);
        }).timeout(30000);

        it('load at start', async () => {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({query: {
                package_name: 'gitlab:node-microservice/template',
                package_dir: 'microservice-template',
                run_args: JSON.stringify({port: port}),
                keepalive: false
            }});
            const res = await request(service.server).get('/spawn' + query).expect(200);
            const pid = JSON.parse(res.text).data.pid;
            assert.notEqual(pid, null);
            const endpoint = url.format({
                port: port,
                protocol: 'http',
                hostname: '127.0.0.1',
                pathname: '/'
            });

            // Test the service
            const res2 = await Requests.get(endpoint);
            assert.equal(res2, 'Hello World');

            // Kill the microservice
            console.log('Killing process...');
            process.kill(pid);

            // Let the service start again by itself
            service.stop();
            service.db.run(`UPDATE spawned SET keepalive = 1 WHERE pid = ?`, [pid]);
            service = await main();

            // Give the microservice a few seconds to start up
            console.log('Waiting for process to restart...')
            await new Promise((resolve, _) => setTimeout(resolve, 5000));

            // Test the service again
            const res3 = await Requests.get(endpoint);
            assert.equal(res3, 'Hello World');
        }).timeout(120000);

    });

    describe('kill', () => {

        it('keep alive', async () => {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({query: {
                package_name: 'gitlab:node-microservice/template',
                package_dir: 'microservice-template',
                run_args: JSON.stringify({port: port})
            }});
            const res = await request(service.server).get('/spawn' + query).expect(200);
            const pid = JSON.parse(res.text).data.pid;
            const endpoint = url.format({
                port: port,
                protocol: 'http',
                hostname: '127.0.0.1',
                pathname: '/'
            });

            // Give the microservice a few seconds to start up
            //await new Promise((resolve, _) => setTimeout(resolve, 5000));

            // Test the service
            const res2 = await Requests.get(endpoint);
            assert.equal(res2, 'Hello World');

            // Kill the microservice
            const res3 = await request(service.server).get('/kill?pid=' + pid).expect(200);

            // Give the microservice a few seconds to start up
            console.log('Waiting for process to restart...')
            await new Promise((resolve, _) => setTimeout(resolve, 5000));
                    
            // Test the service again
            const res4 = await Requests.get(endpoint);
            assert.equal(res4, 'Hello World');
        }).timeout(120000);
    });
});