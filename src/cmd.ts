import { ChildProcess, spawn, fork } from 'child_process';
import * as which from 'which';

export type SpawnedProcess = {proc: ChildProcess, module: string, pid: number, args: string[], timestamp: number};

export function run(bin: string, args: string[]): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        which(bin, (err, binpath) => {
            if (err) {
                reject(new Error(`"${bin}" could not be found.`));
            } else {
                const child = spawn(bin, args, {shell: true, stdio: ['ipc'], cwd: __dirname});
                child.on('close', code => {
                    if (code !== 0) {
                        reject(new Error(`"${bin}" exited with non-zero code ${code}`));
                    } else {
                        resolve();
                    }
                });
            }
        });
    });
}

export function load(module: string, args: string[], timeout = 15000): Promise<SpawnedProcess> {
    return new Promise<SpawnedProcess>((resolve, reject) => {
        let finished = false;
        let spawned: SpawnedProcess;
        const child = fork(module, args, {stdio: [0, 1, 2, 'ipc'], cwd: __dirname});
        
        child.on('error', err => {
            if (!finished) {
                finished = true;
                reject(err);
            }
        });
        child.on('disconnect', () => {
            if (!finished) {
                finished = true;
                spawned = {
                    proc: child,
                    module: module,
                    pid: child.pid,
                    args: args,
                    timestamp: new Date().getTime()
                };
                resolve(spawned);
            }
        });
        child.on('close', code => {
            if (!finished) {
                finished = true;
                reject(new Error(`"${module}" exited prematurely with code ${code}`));
            }
        });
        setTimeout(() => {
            if (!finished) {
                finished = true;
                child.kill();
                return reject(new Error(`"${module}" timed out before signaling start so it was killed`));
            }
        }, timeout);
    });
}