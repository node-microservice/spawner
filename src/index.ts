import { MicroService } from 'microservice';
import { SpawnedProcess, run, load } from './cmd';
import * as minimist from 'minimist';
import * as url from 'url';
import * as path from 'path';

/**
 * Main entrypoint to start this service
 * @param opts Catch-all for parameters. Options include:
 * 
 * `port` - TCP port where this service should run
 * 
 * `db` - database location, defaults to in-memory database
 * 
 * `orchestrator` - URL where microservice-orchestrator is running; must be full URL including
 * protocol. E.g. "http://127.0.0.1:3000"
 */
export async function main(opts: {[key: string]: any} = {}): Promise<MicroService> {

    // Parse command-line arguments
    const GLOBAL_STATS: {[key: string]: any} = {};
    const args = minimist(process.argv.slice(1));

    // Helper function used to read arguments. The order of preference is: 1) opts 2) args 3) env
    function getopt(name: string, env = true): string {
        return opts[name] || args[name] || (env && process.env[name]);
    }

    // Initialize microservice
    const service = await MicroService.create(getopt('db'));

    // Setup debugging
    process.on('unhandledRejection', err => service.log('E', err));

    // Initialize services database
    await service.db.run(`
        CREATE TABLE IF NOT EXISTS spawned (
            package_name      TEXT NOT NULL,
            package_dir       TEXT,
            module_path       TEXT NOT NULL,
            args              TEXT NOT NULL,
            pid               INTEGER NOT NULL,
            keepalive         INTEGER DEFAULT 1,
            timestamp         DATETIME DEFAULT CURRENT_TIMESTAMP)`);

    /**
     * Helper function used to spawn a process given its npm package name
     * @param package_name 
     * @param run_args 
     * @param keepalive 
     * @param package_dir 
     */
    async function spawn(package_name: string, run_args: any, keepalive: boolean, package_dir?: string) {

        // Define binary names and arguments
        const npm_bin = 'npm';
        const npm_args = ['install', '--prefix', __dirname, package_name];
        await run(npm_bin, npm_args);

        // Log success and proceed
        let msg = `Package ${package_name} successfully installed`;
        service.log('V', msg);
        if (run_args.orchestrator || getopt('orchestrator')) {
            run_args.orchestrator = run_args.orchestrator || getopt('orchestrator');
        }
        //require(request.query.package_name).main(run_args);
        const package_path = package_dir || package_name.split('/').pop();
        const module_path = path.join(__dirname, 'node_modules', package_path, 'dist', 'index.js');
        const run_args_ = Object.keys(run_args).reduce((acc, key) => {
            return acc.concat([`--${key}`, run_args[key]]);
        }, []);
        service.log('V', module_path, run_args_);

        const spawned = await load(module_path, run_args_);

        // Add the spawned process to database
        await service.db.run(`
            INSERT INTO spawned (package_name, package_dir, module_path, args, pid, keepalive) 
            VALUES (?, ?, ?, ?, ?, ?)`,
            [package_name, package_dir, spawned.module, JSON.stringify(run_args), spawned.pid, keepalive]);

            if (keepalive) {
                spawned.proc.on('exit', async function exitHandler(code, signal) {
                    const msg = `Process ${spawned.module} [${spawned.pid}] died`;
                    service.log('E', msg);
                    const old_pid = spawned.proc.pid;
                    try {
                        const spawned_ = await load(spawned.module, spawned.args);
                        spawned_.proc.on('exit', exitHandler);
                        await service.db.run(`UPDATE spawned SET pid = ? WHERE pid = ?`,
                            [spawned_.pid, old_pid]);
                    } catch (err) {
                        const msg = `Error respawning dead process [${old_pid}] for ${package_name}`;
                        service.log('E', msg, err.message);
                    }
                });
            }

            // Return the PID and success message
            const pid = spawned.pid;
            msg = `Successfully spawned MicroService "${package_name}"`;
            service.log('I', msg, `[${pid}]`);
            return pid;
    }

    // Spawned processes if there were any left to start
    await service.db.each(`
        SELECT package_name, package_dir, module_path, args, pid FROM spawned WHERE keepalive = 1`,
        async (err, row) => {
            if (err || !row) {
                const msg = 'Error querying database';
                service.log('E', msg, err);
            } else {
                try {
                    const pid = await spawn(row.package_name, JSON.parse(row.args), true, row.package_dir);
                    service.db.run(`DELETE FROM spawned WHERE pid = ?`, row.pid);
                    const msg = `Service for ${row.package_name} successfully restarted [${pid}]`;
                    service.log('I', msg);
                } catch(err) {
                    // Nothing we can do here...
                    service.log('E', 'Error spawning service during startup.', err);
                }
            }
    });


    // Routes setup

    // Status
    // Important: do not delete this route if you plan on using orchestrator!
    await service.route('/status', (request, response) => {
        response.send({status: 'OK', data: GLOBAL_STATS});
    });

    await service.route('/spawn', async (request, response) => {
        // Helper function used to catch promise rejections and return
        const catcher = (err: Error) => {
            const msg = `Error spawning process`;
            response.status(500).send(msg);
            service.log('E', msg, err.message);
        }

        const run_args = JSON.parse(request.query.run_args || '{}');
        const keepalive = !request.query.hasOwnProperty('keepalive') || !!request.query.keepalive;
        const package_dir = request.query.package_dir;
        try {
            const pid = await spawn(request.query.package_name, run_args, keepalive, package_dir);
            const msg = `Successfully spawned MicroService "${request.query.package_name}"`;
            response.send({status: 'OK', data: {message: msg, pid: pid}});
        } catch(err) {
            const msg = `Error spawning process`;
            response.status(500).send(msg);
            service.log('E', msg, err);
        }
    }, {
        method: 'GET',
        mandatoryQueryParameters: ['package_name'],
        optionalQueryParameters: ['run_args', 'package_dir', 'keepalive']
    });

    await service.route('/kill', async (request, response) => {
        try {
            const row = await service.db.get(`
                SELECT package_name, module_path, args, pid, keepalive FROM spawned
                WHERE pid = ?`, [request.query.pid]);

            if (!row) {
                const msg = `Process with PID ${request.query.pid} not found`;
                service.log('E', msg);
                response.status(400).send(msg);

            } else {
                process.kill(row.pid);
                await service.db.run(`DELETE FROM spawned WHERE pid = ?`, [request.query.pid]);
                const msg = `Process ${row.pid} belonging to ${row.package_name} successfully killed`;
                response.send({status: 'OK', data: msg});
            }

        } catch(err) {
            const msg = `Error killing process ${request.query.pid}`;
            service.log('E', msg, err);
            response.status(500).send(msg);
        }
    }, {method: 'GET', mandatoryQueryParameters: ['pid']});
    
    // Maintenance loop
    const maintenance_loop = async () => {
        const rows = await service.db.all(
            `SELECT timestamp, message FROM log WHERE level = 'E' ORDER BY timestamp DESC LIMIT 10`);
        const msgs = rows.map((row: any) => row.message);
        GLOBAL_STATS.last_errors = rows
            .filter((row: any, ix) => msgs.indexOf(row.message) === ix)
            .map((row: any) => `[${row.timestamp}] ${row.message}`);
        GLOBAL_STATS.spawned_processes = await service.db.all(
            `SELECT package_name, args, pid, keepalive FROM spawned`);

        // Register with orchestrator if option was passed as argument
        if (getopt('orchestrator')) {
            const orchestrator_url = url.parse(getopt('orchestrator'));
            service.register(orchestrator_url, 'spawner', '/spawn');
            service.register(orchestrator_url, 'spawner', '/kill');
        }
    };
    setInterval(maintenance_loop, 60000);
    await maintenance_loop();

    // Pick a random port
    service.start(parseInt(getopt('port')) || Math.floor(Math.random() * 8999) + 1000);
    
    // Notify other components of startup, but do not exit on failure
    await service.notify().catch(err => { /* already logged by notify() */ });
    
    return service;
}

// If starting from the command line, begin execution
if (typeof require !== 'undefined' && require.main === module) {
    main();
}
