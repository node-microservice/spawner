"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ootils_1 = require("ootils");
const assert = require("assert");
const url = require("url");
const request = require("supertest");
const index_1 = require("../index");
let service;
describe('routes', () => {
    before(() => __awaiter(this, void 0, void 0, function* () {
        service = yield index_1.main();
    }));
    beforeEach(() => {
        service.stop();
        service.clearCache(true);
        service.start(Math.floor(Math.random() * 8999) + 1000);
    });
    afterEach(() => {
        service.stop();
    });
    describe('spawn', () => {
        it('microservice-template', () => __awaiter(this, void 0, void 0, function* () {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({ query: {
                    package_name: 'gitlab:node-microservice/template',
                    package_dir: 'microservice-template',
                    run_args: JSON.stringify({ port: port })
                } });
            const res = yield request(service.server).get('/spawn' + query).expect(200);
            const pid = JSON.parse(res.text).data.pid;
            assert.notEqual(pid, null);
            const endpoint = url.format({
                port: port,
                protocol: 'http',
                hostname: '127.0.0.1',
                pathname: '/'
            });
            // Give the microservice a few seconds to start up
            console.log('Waiting for process to restart...');
            yield new Promise((resolve, _) => setTimeout(resolve, 5000));
            // Test the service
            const res2 = yield ootils_1.Requests.get(endpoint);
            assert.equal(res2, 'Hello World');
        })).timeout(120000);
        it('keep alive', () => __awaiter(this, void 0, void 0, function* () {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({ query: {
                    package_name: 'gitlab:node-microservice/template',
                    package_dir: 'microservice-template',
                    run_args: JSON.stringify({ port: port })
                } });
            const res = yield request(service.server).get('/spawn' + query).expect(200);
            const pid = JSON.parse(res.text).data.pid;
            assert.notEqual(pid, null);
            const endpoint = url.format({
                port: port,
                protocol: 'http',
                hostname: '127.0.0.1',
                pathname: '/'
            });
            // Give the microservice a few seconds to start up
            console.log('Waiting for process to restart...');
            yield new Promise((resolve, _) => setTimeout(resolve, 5000));
            // Test the service
            const res2 = yield ootils_1.Requests.get(endpoint);
            assert.equal(res2, 'Hello World');
            // Kill the microservice
            console.log('Killing process...');
            process.kill(pid);
            // Give the microservice a few seconds to start up
            console.log('Waiting for process to restart...');
            yield new Promise((resolve, _) => setTimeout(resolve, 5000));
            // Test the service again
            const res3 = yield ootils_1.Requests.get(endpoint);
            assert.equal(res3, 'Hello World');
        })).timeout(120000);
        it('keep alive not', () => __awaiter(this, void 0, void 0, function* () {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({ query: {
                    package_name: 'gitlab:node-microservice/template',
                    package_dir: 'microservice-template',
                    run_args: JSON.stringify({ port: port }),
                    keepalive: null
                } });
            const res = yield request(service.server).get('/spawn' + query).expect(200);
            const pid = JSON.parse(res.text).data.pid;
            assert.notEqual(pid, null);
            const endpoint = url.format({
                port: port,
                protocol: 'http',
                hostname: '127.0.0.1',
                pathname: '/'
            });
            // Test the service
            const res2 = yield ootils_1.Requests.get(endpoint);
            assert.equal(res2, 'Hello World');
            // Kill the microservice
            console.log('Killing process...');
            process.kill(pid);
            // Give the microservice a few seconds to start up
            console.log('Waiting for process to restart...');
            yield new Promise((resolve, _) => setTimeout(resolve, 5000));
            // Test the service again
            let errored = false;
            try {
                const res3 = yield ootils_1.Requests.get(endpoint);
            }
            catch (err) {
                errored = true;
            }
            finally {
                assert.equal(errored, true);
            }
        })).timeout(120000);
        it('non-existent module', () => __awaiter(this, void 0, void 0, function* () {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({ query: {
                    package_name: 'asdfasdf:asdfasdf',
                    run_args: JSON.stringify({ port: port })
                } });
            yield request(service.server).get('/spawn' + query).expect(500);
        })).timeout(30000);
        it('load at start', () => __awaiter(this, void 0, void 0, function* () {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({ query: {
                    package_name: 'gitlab:node-microservice/template',
                    package_dir: 'microservice-template',
                    run_args: JSON.stringify({ port: port }),
                    keepalive: false
                } });
            const res = yield request(service.server).get('/spawn' + query).expect(200);
            const pid = JSON.parse(res.text).data.pid;
            assert.notEqual(pid, null);
            const endpoint = url.format({
                port: port,
                protocol: 'http',
                hostname: '127.0.0.1',
                pathname: '/'
            });
            // Test the service
            const res2 = yield ootils_1.Requests.get(endpoint);
            assert.equal(res2, 'Hello World');
            // Kill the microservice
            console.log('Killing process...');
            process.kill(pid);
            // Let the service start again by itself
            service.stop();
            service.db.run(`UPDATE spawned SET keepalive = 1 WHERE pid = ?`, [pid]);
            service = yield index_1.main();
            // Give the microservice a few seconds to start up
            console.log('Waiting for process to restart...');
            yield new Promise((resolve, _) => setTimeout(resolve, 5000));
            // Test the service again
            const res3 = yield ootils_1.Requests.get(endpoint);
            assert.equal(res3, 'Hello World');
        })).timeout(120000);
    });
    describe('kill', () => {
        it('keep alive', () => __awaiter(this, void 0, void 0, function* () {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({ query: {
                    package_name: 'gitlab:node-microservice/template',
                    package_dir: 'microservice-template',
                    run_args: JSON.stringify({ port: port })
                } });
            const res = yield request(service.server).get('/spawn' + query).expect(200);
            const pid = JSON.parse(res.text).data.pid;
            const endpoint = url.format({
                port: port,
                protocol: 'http',
                hostname: '127.0.0.1',
                pathname: '/'
            });
            // Give the microservice a few seconds to start up
            //await new Promise((resolve, _) => setTimeout(resolve, 5000));
            // Test the service
            const res2 = yield ootils_1.Requests.get(endpoint);
            assert.equal(res2, 'Hello World');
            // Kill the microservice
            const res3 = yield request(service.server).get('/kill?pid=' + pid).expect(200);
            // Give the microservice a few seconds to start up
            console.log('Waiting for process to restart...');
            yield new Promise((resolve, _) => setTimeout(resolve, 5000));
            // Test the service again
            const res4 = yield ootils_1.Requests.get(endpoint);
            assert.equal(res4, 'Hello World');
        })).timeout(120000);
    });
});
