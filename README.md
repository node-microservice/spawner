# MicroService Spawner

Spawns other microservices given their npm package name, Gitlab or Github location.

## Installation

Installing this package globally also installs a SystemD template unit file. To start the SystemD service then, run the following commands as sudo:

    npm install --global gitlab:node-microservice/spawner --unsafe-perm
    systemctl daemon-reload
    systemctl enable microservice-spawner@1234
    systemctl start microservice-spawner@1234

Substitute 1234 with the port under which you want to run the microservice spawner.

## Usage

The microservice spawner only provider two endpoints to interact with:
- `/spawn`: GET endpoint used to spawn a new microservice that takes the following parameters:
    + `package_name`: argument passed to `npm install`, which can be an npm package, Gitlab or Gitbub URL.
    + `package_dir`: (optional) use only if package name and directory under node_modules do not match.
    + `run_args`: (optional) arguments passed to `npm start <package_name>`.
    + `keepalive`: (optional) monitor status of spawned process and restart if it dies; defaults to true.
- `/kill`: GET endpoint used to kill a running microservice that takes the following parameters:
    + `pid`: PID of the microservice previously spawned by this spawner; it cannot be used to kill arbitrary PIDs.